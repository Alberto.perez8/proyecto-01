package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E> implements List<E> {

	public class Node<E>{

		private Node<E> next;
		private E element;

		public Node() {
			this.element = null;
			this.next = null;
		}

		public Node(E elmnt) {
			this.element = elmnt;
			this.next = null;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

	}

	public class ListIterator<E> implements Iterator<E>{

		private Node<E> currentNode;

		public ListIterator(){
			this.currentNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return this.currentNode != null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.currentNode.getElement();
				this.currentNode = this.currentNode.getNext();
				return result;
			}
			throw new NoSuchElementException();
		}


	}

	private int size;
	private Node<E> header;

	public SinglyLinkedList() {
		this.size = 0;
		this.header = new Node<E>();
	}

	@Override
	public Iterator<E> iterator() {
		return new ListIterator<E>();
	}


	@Override
	public void add(E obj) {
		Node<E> NewNode = new Node<E>(obj);
		if(this.isEmpty()) { 
			this.header.setNext(NewNode);
		}
		else {
			Node<E> last = this.getNode(this.size-1);
			last.setNext(NewNode);
		}
		this.size++;
	}

	@Override
	public void add(int index, E obj) {
		this.checkIndex(index);
		if(this.isEmpty()) this.add(obj);
		else {
			Node<E> NewNode = new Node<E>(obj);
			Node<E> prev = this.getNode(index - 1);
			NewNode.setNext(prev.getNext());
			prev.setNext(NewNode);
			this.size++;
		}
	}

	@Override
	public boolean remove(E obj) {
		if(this.isEmpty()) return false;

		Node<E> prev = this.header;
		while(prev.getNext().getElement() != obj) {
			prev = prev.getNext();
		}
		Node<E> target = prev.getNext();
		if(target != null && target.getElement().equals(obj)){
			prev.setNext(target.getNext());
			target.setElement(null);
			target.setNext(null);
			this.size--;
			return true;
		}

		else {
			return false;
		}
	}

	@Override
	public boolean remove(int index) {
		this.checkIndex(index);
		if(index == 0) {
			Node<E> target = this.header.getNext();
			this.header.setNext(target.getNext());
			target.setElement(null);
			target.setNext(null);
			this.size--;
			return true;
		}
		if(index > 0) {
			Node<E> prev = this.getNode(index - 1);
			Node<E> target = this.getNode(index);
			prev.setNext(target.getNext());
			target.setElement(null);
			target.setNext(null);
			this.size--;
			return true;
		}
		return false;
	}

	@Override
	public int removeAll(E obj) {
		if(this.isEmpty()) return 0;
		int counter = 0;
		for(int i = 0; i < this.size; i++) {
			if(this.getNode(i).getElement().equals(obj)) {
				this.remove(i);
				counter++;
			}
		}
		return counter;
	}

	@Override
	public E get(int index) {
		return this.getNode(index).getElement();
	}

	@Override
	public E set(int index, E obj) {
		Node<E> temp = this.getNode(index);
		E result =  temp.getElement();
		temp.setElement(obj);
		return result;
	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		return this.getNode(this.size-1).getElement();
	}

	@Override
	public int firstIndex(E obj) {
		for(int i = 0; i < this.size; i++) {
			if(this.get(i).equals(obj)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		for(int i = this.size-1; i > 0; i--) {
			if(this.get(i).equals(obj)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public boolean contains(E obj) {
		for(E i: this) {
			if(i.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	/**
	 * Searches the node for the given index, starts from 0 to size and 
	 * the initial node is the one after the dummy header.
	 * @param index - index in the list to find
	 * @return node found
	 */
	public Node<E> getNode(int index){
		Node<E> result = this.header.getNext();
		int count = 0;
		while(count < index) {
			result = result.getNext();
			count++;
		}
		return result;
	}

	/** 
	 * Verifies if the index is in the correct bounds
	 * @param index - Index to be verified.
	 */
	public void checkIndex(int index) {
		if(index < 0 || index > this.size) {
			throw new IndexOutOfBoundsException();
		}
	}
}
