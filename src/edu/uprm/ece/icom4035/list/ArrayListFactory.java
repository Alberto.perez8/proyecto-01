package edu.uprm.ece.icom4035.list;

import edu.uprm.ece.icom4035.polynomial.Term;

public class ArrayListFactory<T> implements ListFactory<Term> {

	@Override
	public List<Term> newInstance() {
		return new ArrayList<Term>();
	}

}
