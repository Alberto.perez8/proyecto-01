package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E>{

	private class ListIterator<E> implements Iterator<E>{

		private int currentPosition;
		
		public ListIterator() {
			this.currentPosition = 0;
		}
		
		@Override
		public boolean hasNext() {
			return this.currentPosition < size();
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = (E) elements[this.currentPosition];
				currentPosition++;
				return result;
			}
			throw new NoSuchElementException();
		}
		
	}
	
	private E[] elements;
	private int size;
	private int INIT_CAP = 5;

	public ArrayList() {
		this.elements = (E[]) new Object[INIT_CAP];
		this.size = 0;
	}

	public ArrayList(E[] elements) {
		this.elements = elements;
		this.size = elements.length;
	}


	@Override
	public Iterator<E> iterator() {
		return new ListIterator<E>();
	}

	@Override
	public void add(E obj) {
		if(this.size == this.elements.length) {
			this.changeCapacity();
		}
		this.elements[this.size] = obj;
		this.size++;
	}

	@Override
	public void add(int index, E obj) {
		this.checkIndex(index);
		if(this.size == this.elements.length) {
			this.changeCapacity();
		}

		if(this.size == index) {
			this.add(obj);
			return;
		}

		else {
			for(int i = this.size; i > index; i--) {
				this.elements[i] = this.elements[i - 1];
			}
			this.elements[index] = obj;
			this.size++;
		}
	}


	@Override
	public boolean remove(E obj) { 
		if(this.isEmpty()) return false;
		for(int i = 0; i< this.size;i++) {
			if(this.elements[i] == obj) {
				this.remove(i);
			}
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		this.checkIndex(index);
		if(this.isEmpty()) return false;
		for(int i = index; i< this.size; i++) {
			this.elements[i] = this.elements[i+1];
		}
		this.elements[this.size - 1] = null;
		this.size--;
		return true;

	}

	@Override
	public int removeAll(E obj) { 
		if(this.isEmpty()) return 0;
		int counter = 0;
		for(int i = 0; i < this.size; i++) {
			if(this.elements[i].equals(obj)) {
				this.remove(this.elements[i]);
				this.size--;
				counter++;
			}
		}
		return counter;
	}

	@Override
	public E get(int index) {
		this.checkIndex(index);
		return this.elements[index];
	}

	@Override
	public E set(int index, E obj) {
		this.checkIndex(index);
		E result = this.get(index);
		this.elements[index] = obj;
		return result;
	}

	@Override
	public E first() {
		if(this.isEmpty()) return null;
		return this.elements[0];
	}

	@Override
	public E last() {
		if(this.isEmpty()) return null;
		return this.elements[this.size-1];
	}

	@Override
	public int firstIndex(E obj) {
		for(int i = 0; i < this.size; i++) {
			if(this.elements[i].equals(obj)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		for(int i = this.size; i <= 0; i--) {
			if(this.elements[i].equals(obj)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public boolean contains(E obj) {
		for(E i: this) {
			if(i.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	private void changeCapacity() {
		E[] newArray = (E[]) new Object[this.size * 2];
		for(int i = 0; i < this.size; i++) {
			newArray[i] = this.elements[i];
		}
		this.elements = newArray;
	}

	private void checkIndex(int index) {
		if(index < 0 || index > this.size) 
			throw new IndexOutOfBoundsException();
	}
}
