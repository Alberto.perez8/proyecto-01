package edu.uprm.ece.icom4035.polynomial;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.List;

public class PolynomialImp implements Polynomial {

	private class TermIterator<Term> implements Iterator<Term>{

		private int currentIndex;


		public TermIterator() {
			this.currentIndex = 0;
		}

		@Override
		public boolean hasNext() {
			return (currentIndex < List.size());
		}

		@Override
		public Term next() {
			while(this.hasNext()) {
				Term result = (Term) List.get(currentIndex);
				this.currentIndex++;
				return result;
			}
			throw new NoSuchElementException();
		}


	}

	private List<Term> List;

	public PolynomialImp() {
		this.List = (List<Term>) TermListFactory.newListFactory().newInstance();
		for(Term t: this.List) {
			
				this.List.remove(t);
			}
		}

	public PolynomialImp(String expression) { 
		this.List = (List<Term>) TermListFactory.newListFactory().newInstance();
		String [] splitExp = expression.split("\\+");
		double coefficient;
		int exponent;
		for(String i: splitExp) {
			if(i.contains("x")) {
				int posX = i.indexOf("x");
				if(posX == 0) {
					coefficient = 1;
				}
				else {
					coefficient = Double.parseDouble(i.substring(0,posX));
				}
				if(i.contains("^")){
					exponent = Integer.parseInt(i.substring(posX + 2));
				}
				else {
					exponent = 1;
				}
				this.List.add(new TermImp(coefficient,exponent));
			}
			else {
				coefficient = Double.parseDouble(i);
				this.List.add(new TermImp(coefficient, 0));
			}
		}

	}

	@Override
	public Iterator<Term> iterator() {
		return new TermIterator<Term>();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		List<Term> thisList = this.List;
		PolynomialImp p2 = (PolynomialImp) P2;
		List<Term> P2List = p2.List;
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.List;
		int i = 0;
		int j = 0;

		while(i < thisList.size() && j < P2List.size()) {
			Term ATerm = thisList.get(i);
			Term BTerm = P2List.get(j);
			double coefficient = 0;			
			if(ATerm.getExponent() == BTerm.getExponent()) {
				coefficient = ATerm.getCoefficient() + BTerm.getCoefficient();
				resultList.add(new TermImp(coefficient, ATerm.getExponent()));
				i++;
				j++;
			}
			
			if(ATerm.getExponent() != BTerm.getExponent()) {
				if(ATerm.getExponent() > BTerm.getExponent()) {
					resultList.add(ATerm);
					i++;
				}
				resultList.add(BTerm);
				j++;
			}
		}

		if(i< thisList.size() && j>= P2List.size()) {
			for(int l = i; l<thisList.size(); l++) {
				resultList.add(thisList.get(l));
			}
		}
		else {
			for(int l = i; l<P2List.size(); l++) {
				resultList.add(P2List.get(l));
			}
		}
		

		return result;
	}


	@Override
	public Polynomial subtract(Polynomial P2) {
		List<Term> thisList = this.List;
		PolynomialImp z = (PolynomialImp) P2;
		List<Term> P2List = z.List;
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.List;
		int i = 0;
		int j = 0;

		while(i < thisList.size() && j < P2List.size()) {
			Term ATerm = thisList.get(i);
			Term BTerm = P2List.get(j);
			
			if(ATerm.getExponent() == BTerm.getExponent()) {
				double coefficient = 0;
				coefficient = ATerm.getCoefficient() - BTerm.getCoefficient();
				resultList.add(new TermImp(coefficient, ATerm.getExponent()));
				i++;
				j++;
			}
	
			if(ATerm.getExponent() != BTerm.getExponent()) {
				if(ATerm.getExponent() > BTerm.getExponent()) {
					resultList.add(ATerm);
					i++;
				}
					resultList.add(new TermImp(-BTerm.getCoefficient(), BTerm.getExponent()));
					j++;
			}
		}

		if(i< thisList.size()) {
			for(int l = i; l<thisList.size(); l++) {
				resultList.add(thisList.get(l));
			}
		}
		else {
			for(int l = i; l<P2List.size(); l++) {
				resultList.add(new TermImp(-P2List.get(l).getCoefficient(), P2List.get(l).getExponent()));
			}
		}

		return result;
	}


	@Override
	public Polynomial multiply(Polynomial P2) {
		PolynomialImp P = (PolynomialImp) P2;
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.List;
		List<Term> p2List = P.List;
		for(Term i: this.List) {
			for(Term j: p2List) {
				resultList.add(new TermImp(i.getCoefficient()*j.getCoefficient(),
						i.getExponent() + j.getExponent()));
			}
		}
		int currpos = 0;
		for(int i = 1; i<resultList.size(); i++) {
			if(resultList.get(currpos).getExponent() == resultList.get(i).getExponent()) {
				resultList.set(currpos, new TermImp(resultList.get(currpos).getCoefficient()
						+ resultList.get(i).getCoefficient(), resultList.get(currpos).getExponent()));
				resultList.remove(i);
			}
		}
		
		
		return result;
	}

	@Override
	public Polynomial multiply(double c) {
		List<Term> tList = this.List;
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.List;
		for(Term i: tList) {
			resultList.add(new TermImp(i.getCoefficient() * c, i.getExponent()));
		}
	
		return result;
	}

	@Override
	public Polynomial derivative() {
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.List;
		for(Term i: this.List) {
			resultList.add(new TermImp(i.getCoefficient()* i.getExponent(), i.getExponent()-1));
		}
		
		return result;
	}

	@Override
	public Polynomial indefiniteIntegral() {
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.List;
		for(Term i: this.List) {
			resultList.add(new TermImp(i.getCoefficient()/(i.getExponent()+1), i.getExponent()+1));
		}
		resultList.add(new TermImp(1, 0));
		
		return result;
	}

	@Override
	public double definiteIntegral(double a, double b) {
		PolynomialImp integral = (PolynomialImp) this.indefiniteIntegral();
		double valA = integral.evaluate(a);
		double valB = integral.evaluate(b);
		double result = valB - valA;

		return result;
	}

	@Override
	public int degree() {
		return this.List.first().getExponent();
	}

	@Override
	public double evaluate(double x) {
		double result = 0;
		for(Term i: this.List) {
			result += i.evaluate(x);
		}
		return result;
	}

	@Override
	public boolean equals(Polynomial P) {
		PolynomialImp p = (PolynomialImp) P;
		for(Term i: this.List) {
			for(Term j: p.List) {
				if(i.getCoefficient() == j.getCoefficient() && 
						i.getExponent() == j.getExponent()) return true;
			}
		}
		return false;
	}

	public List<Term> getList() {
		return List;
	}

	public void setList(List<Term> list) {
		List = list;
	}

	@Override
	public String toString() {
		String result = "";
		for(int b = 0; b <this.List.size() -1; b++) {
			result += this.List.get(b).toString() + "+";
		}
		result += this.List.get(this.List.size()-1);
		return result;
	}


}
