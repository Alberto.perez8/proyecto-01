package edu.uprm.ece.icom4035.polynomial;

import java.text.DecimalFormat;

public class TermImp implements Term {

	private double coefficient;
	private int exponent;
	
	public TermImp() {
		this.coefficient = 0;
		this.exponent = 0;
	}
	
	public TermImp(double coefficient, int exponent) {
		this.coefficient = coefficient;
		this.exponent = exponent;
	}
	
	
	
	@Override
	public double getCoefficient() {
		return this.coefficient;
	}

	@Override
	public int getExponent() {
		return this.exponent;
	}

	@Override
	public double evaluate(double x) {
		return this.coefficient * Math.pow(x, this.exponent);
	}
	
	@Override
	public String toString() {
		String exp = "";
		DecimalFormat df = new DecimalFormat("#.00");
		if(this.getExponent() > 0) {
			exp ="x";
			if(this.getExponent() > 1) exp += "^" + Integer.toString(this.getExponent());
		}
		String coef = df.format(this.getCoefficient());
		return coef + exp;
	}

}
